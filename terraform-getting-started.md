# Getting Started with Terraform

Terraform is a popular tool for defining and provisioning [infrastructure as code](https://learn.hashicorp.com/terraform/getting-started/intro#infrastructure-as-code) (IaC). This guide will use the command line to walk you through some of the basics. By the end of this guide, you will have installed Terraform, as well as created and destroyed a webserver using Terraform.

## Prerequisites

- Terraform 0.12.0 or later
- Basic familiarity with the command line
- [Docker](https://docs.docker.com/engine/install/)

## Install Terraform

To install Terraform, visit [Terraform.io](https://www.terraform.io/downloads.html) and find the package for your preferred operating system. The package will download as a zip archive. If you need assistance installing Terraform, visit [Getting Started - Install Terraform](https://learn.hashicorp.com/terraform/getting-started/install).

With Terraform installed, let's dive right into it and start creating some infrastructure.

## Create Infrastructure

First, let's create a new directory on your local machine and then create a Terraform configuration code inside it.

```sh
$ mkdir terraform-demo
$ cd terraform-demo

~/terraform-demo
```

Next, create a file for your Terraform configuration code.

```sh
$ touch main.tf
```

Paste the following lines into the file.

```hcl
provider "docker" {
    host = "unix:///var/run/docker.sock"
}

resource "docker_container" "nginx" {
  image = docker_image.nginx.latest
  name  = "training"
  ports {
    internal = 80
    external = 80
  }
}

resource "docker_image" "nginx" {
  name = "nginx:latest"
}
```

Initialize Terraform with the `init` command. The Docker provider will be installed.

```sh
$ terraform init

Initializing the backend...

Initializing provider plugins...
- Checking for available provider plugins...
- Downloading plugin for provider "docker" (terraform-providers/docker) 2.7.0...

The following providers do not have any version constraints in configuration,
so the latest version was installed.

To prevent automatic upgrades to new major versions that may contain breaking
changes, it is recommended to add version = "..." constraints to the
corresponding provider blocks in configuration, with the constraint strings
suggested below.

* provider.docker: version = "~> 2.7"

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
```

You should check for any errors. If the command ran successfully, provision the resource with the `apply` command.

```sh
$ terraform apply

An execution plan has been generated and is shown below.
Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # docker_container.nginx will be created
  + resource "docker_container" "nginx" {
      + attach           = false
      + bridge           = (known after apply)
      + command          = (known after apply)
      + container_logs   = (known after apply)
      + entrypoint       = (known after apply)
      + env              = (known after apply)
      + exit_code        = (known after apply)
      + gateway          = (known after apply)
      + hostname         = (known after apply)
      + id               = (known after apply)
      + image            = (known after apply)
      + ip_address       = (known after apply)
      + ip_prefix_length = (known after apply)
      + ipc_mode         = (known after apply)
      + log_driver       = "json-file"
      + logs             = false
      + must_run         = true
      + name             = "training"
      + network_data     = (known after apply)
      + read_only        = false
      + restart          = "no"
      + rm               = false
      + shm_size         = (known after apply)
      + start            = true

      + labels {
          + label = (known after apply)
          + value = (known after apply)
        }

      + ports {
          + external = 80
          + internal = 80
          + ip       = "0.0.0.0"
          + protocol = "tcp"
        }
    }

  # docker_image.nginx will be created
  + resource "docker_image" "nginx" {
      + id     = (known after apply)
      + latest = (known after apply)
      + name   = "nginx:latest"
    }

Plan: 2 to add, 0 to change, 0 to destroy.

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value:

```

Look for a message at the bottom of the output asking for confirmation. Type `yes` and hit ENTER. The command will take up to a few minutes to run and will display a message indicating that the resource was created.

## Destroy infrastructure

Finally, destroy the infrastructure. You will use the `terraform destroy` command to do this. This command is the opposite of `terraform apply` in that it will terminate those resources defined in the configuration file.

```sh
$ terraform destroy

An execution plan has been generated and is shown below.
Resource actions are indicated with the following symbols:
  - destroy

Terraform will perform the following actions:

  # docker_container.nginx will be destroyed
  - resource "docker_container" "nginx" {
      - attach            = false -> null
      - command           = [
          - "nginx",
          - "-g",
          - "daemon off;",
        ] -> null
      - cpu_shares        = 0 -> null
      - dns               = [] -> null
      - dns_opts          = [] -> null
      - dns_search        = [] -> null
      - entrypoint        = [] -> null
      - env               = [
          - "NGINX_VERSION=1.17.10",
          - "NJS_VERSION=0.3.9",
          - "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
          - "PKG_RELEASE=1~buster",
        ] -> null
      - gateway           = "172.17.0.1" -> null
      - group_add         = [] -> null
      - hostname          = "4900387d13f0" -> null
      - id                = "4900387d13f0ce00cc53e84c6160bf2d0d0ae3c6c5ff667a36aa040ceaaeee77" -> null
      - image             = "sha256:602e111c06b6934013578ad80554a074049c59441d9bcd963cb4a7feccede7a5" -> null
      - ip_address        = "172.17.0.2" -> null
      - ip_prefix_length  = 16 -> null
      - ipc_mode          = "private" -> null
      - links             = [] -> null
      - log_driver        = "json-file" -> null
      - log_opts          = {} -> null
      - logs              = false -> null
      - max_retry_count   = 0 -> null
      - memory            = 0 -> null
      - memory_swap       = 0 -> null
      - must_run          = true -> null
      - name              = "training" -> null
      - network_data      = [
          - {
              - gateway          = "172.17.0.1"
              - ip_address       = "172.17.0.2"
              - ip_prefix_length = 16
              - network_name     = "bridge"
            },
        ] -> null
      - network_mode      = "default" -> null
      - privileged        = false -> null
      - publish_all_ports = false -> null
      - read_only         = false -> null
      - restart           = "no" -> null
      - rm                = false -> null
      - shm_size          = 64 -> null
      - start             = true -> null
      - sysctls           = {} -> null
      - tmpfs             = {} -> null

      - labels {
          - label = "maintainer" -> null
          - value = "NGINX Docker Maintainers <docker-maint@nginx.com>" -> null
        }

      - ports {
          - external = 80 -> null
          - internal = 80 -> null
          - ip       = "0.0.0.0" -> null
          - protocol = "tcp" -> null
        }
    }

  # docker_image.nginx will be destroyed
  - resource "docker_image" "nginx" {
      - id     = "sha256:602e111c06b6934013578ad80554a074049c59441d9bcd963cb4a7feccede7a5nginx:latest" -> null
      - latest = "sha256:602e111c06b6934013578ad80554a074049c59441d9bcd963cb4a7feccede7a5" -> null
      - name   = "nginx:latest" -> null
    }

Plan: 0 to add, 0 to change, 2 to destroy.

Do you really want to destroy all resources?
  Terraform will destroy all your managed infrastructure, as shown above.
  There is no undo. Only 'yes' will be accepted to confirm.

  Enter a value: yes


```

Look again for a message at the bottom of the output asking for confirmation. Type `yes` and hit ENTER. Terraform will destroy the resources it created and confirm.

## Next steps

In this guide, you learned how to create and destroy infrastructure using a Terraform configuration file. One of the [advantages](https://learn.hashicorp.com/terraform/getting-started/intro#advantages-of-terraform) of using Terraform is it's platform agnostic - it isn't tied to any one cloud or platform. Let's next explore creating infrastructure in the cloud with AWS. More Terraform "Getting Started" guides including those for other providers such as Azure and Google Cloud are available at [learn.hashicorp.com](https://learn.hashicorp.com/terraform).
